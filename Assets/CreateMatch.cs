﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking.Match;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;

public class CreateMatch : MonoBehaviour {
    private int appId = 113202;

    void Start () {
        NetworkManager manager = GetComponent<NetworkManager>();

        manager.StartMatchMaker();

        manager.matchMaker.SetProgramAppID((AppID)appId);

        manager.matchMaker.CreateMatch("roomname", 2, false, "password", (CreateMatchResponse matchInfo) => {
            Debug.Log("Created game: " + matchInfo.success);
            manager.OnMatchCreate(matchInfo);
        });
    }
}
